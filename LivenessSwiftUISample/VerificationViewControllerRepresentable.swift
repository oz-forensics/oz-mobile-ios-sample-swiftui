//
//  VerificationViewControllerRepresentable.swift
//  LivenessSwiftUISample
//
//  Created by Almir Akmalov on 04.04.2024.
//

import OZLivenessSDK
import UIKit
import SwiftUI
import AVFoundation


struct VerificationViewControllerRepresentable: UIViewControllerRepresentable {
    @Binding var showAlert: Bool
    @Binding var alertText: String
    
    var actions: [OZLivenessSDK.OZVerificationMovement]
    var mode: AnalysisMode
    var referencePhoto: UIImage? = nil
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIViewController(context: Context) -> UIViewController {
        do {
            let viewController = try OZSDK.createVerificationVCWithDelegate(context.coordinator, actions: actions)
            return viewController
        } catch {
            print("VerificationViewController: \(error)")
            return UIViewController()
        }
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        
    }
    
    class Coordinator: NSObject, OZLivenessSDK.OZLivenessDelegate {
        var parent: VerificationViewControllerRepresentable
        
        init(_ parent: VerificationViewControllerRepresentable) {
            self.parent = parent
        }
        
        func onOZLivenessResult(results: [OZLivenessSDK.OZMedia]) {
            self.performAnalysis(results)
        }
        
        func onError(status: OZLivenessSDK.OZVerificationStatus?) {
            switch status {
            case .failedBecauseUserCancelled:
                
                self.parent.alertText = "Cancelled by user"
            case .failedBecauseOfAttemptLimit:
                self.parent.alertText = "Number of attempts exhausted"
            case .failedBecauseOfLicenseError(let error):
                self.parent.alertText = error.localizedDescription
            default:
                self.parent.alertText = "error"
            }
            self.parent.showAlert = true
        }
        
        func performAnalysis(_ results: [OZMedia]) {
            let analysisRequest = AnalysisRequestBuilder()
            
            //`quality` Analysis
            let analysis = Analysis(media: results, type: .quality, mode: parent.mode)
            analysisRequest.addAnalysis(analysis)
            
            //The `referencePhoto` is non-nil only when used within the `BioVerificationView`.
            if let referencePhoto = parent.referencePhoto {
                guard let imageUrl = saveFile(image: referencePhoto) else {
                    return
                }
                
                let referenceMedia = OZMedia(movement: .selfie,
                                             mediaType: .movement,
                                             metaData: nil,
                                             videoURL: imageUrl,
                                             bestShotURL: imageUrl,
                                             preferredMediaURL: nil,
                                             timestamp: Date())
                var analysisMedia = [OZMedia]()
                analysisMedia.append(referenceMedia)
                analysisMedia.append(contentsOf: results)
                
                //`biometry` analysis mode always `.serverBased`
                let analysisBiometry = Analysis(media: analysisMedia, type: .biometry, mode: .serverBased)
                analysisRequest.addAnalysis(analysisBiometry)
            }
            
            analysisRequest.run { status in
                print(status)
            } errorHandler: { error in
                self.parent.showAlert = true
                self.parent.alertText = error.localizedDescription
            } completionHandler: { results in
                let msg = results.analysisResults.map { $0.type }.joined(separator: ", ")
                self.parent.showAlert = true
                self.parent.alertText = results.resolution.rawValue + ", " + msg
            }
        }
        
        private func saveFile(image: UIImage) -> URL? {
            guard let ciImage = CIImage(image: image) else {
                return nil
            }
            
            let filename = getDocumentsDirectory().appendingPathComponent("reference_photo.png")
            try? FileManager.default.removeItem(at: filename)
            savePNG(ciImage.cgImageGen!, url: filename as CFURL)
            return filename
        }
        
        private func savePNG(_ image: CGImage, url: CFURL, properties: CFDictionary? = nil) {
            guard let myImageDest = CGImageDestinationCreateWithURL(url as CFURL, UTType.png.identifier  as CFString, 1, nil) 
            else { return }
            CGImageDestinationAddImage(myImageDest, image, properties)
            CGImageDestinationFinalize(myImageDest)
        }
        
        private func getDocumentsDirectory() -> URL {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            return paths[0]
        }
    }
}

extension CIImage {
    var cgImageGen: CGImage? {
        let context = CIContext(options: nil)
        if let cgImage = context.createCGImage(self, from: self.extent) {
            return cgImage
        }
        return nil
    }
}
