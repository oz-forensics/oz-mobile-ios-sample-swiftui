//
//  EnumAction.swift
//  LivenessSwiftUISample
//
//  Created by Almir Akmalov on 04.04.2024.
//

import OZLivenessSDK

enum Action: String, Identifiable, CaseIterable, CustomStringConvertible {
    case biometricCheck
    case oneShot
    case selfie
    case scan
    case blink
    case smile
    case tiltUp
    case slopeDown
    case turnYourHeadLeft
    case turnYourHeadRight
    case combo
    
    var id: String { self.rawValue }
    
    var description: String {
        switch self {
        case .biometricCheck:
            return "Biometric check"
        case .oneShot:
            return "One shot"
        case .selfie:
            return "Selfie"
        case .scan:
            return "Scan"
        case .blink:
            return "Blink"
        case .smile:
            return "Smile"
        case .tiltUp:
            return "Tilt up"
        case .slopeDown:
            return "Slope down"
        case .turnYourHeadLeft:
            return "Turn your head left"
        case .turnYourHeadRight:
            return "Turn your head right"
        case .combo:
            return "Combo"
        }
    }
}

extension Action {
    func toOZAction() -> OZVerificationMovement? {
        switch self {
        case .oneShot:
            return .one_shot
        case .selfie:
            return .selfie
        case .scan:
            return .scanning
        case .blink:
            return .eyes
        case .smile:
            return .smile
        case .tiltUp:
            return .up
        case .slopeDown:
            return .down
        case .turnYourHeadLeft:
            return .left
        case .turnYourHeadRight:
            return .right
        default:
            return nil
        }
    }
}
