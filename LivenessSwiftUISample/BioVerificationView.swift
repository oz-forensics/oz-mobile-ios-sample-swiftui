//
//  BioVerificationView.swift
//  LivenessSwiftUISample
//
//  Created by Almir Akmalov on 04.04.2024.
//

import SwiftUI
import OZLivenessSDK

struct BioVerificationView: View {
    @State private var image: UIImage?
    @State private var showingImagePicker = false
    @State private var isVerificationPresented = false
    @State private var showAlert = false
    @State private var alertText = ""

    var title: String
    var mode: AnalysisMode
    
    var body: some View {
        VStack {
            if let image = image {
                ZStack(alignment: .topTrailing) {
                    Image(uiImage: image)
                        .resizable()
                        .scaledToFit()

                    Button(action: {
                        self.image = nil
                    }) {
                        Image(systemName: "trash")
                            .padding()
                            .foregroundColor(.white)
                            .background(Color.black.opacity(0.7))
                            .clipShape(Circle())
                    }
                    .padding(.trailing, 10)
                    .padding(.top, 10)
                }
            } else {
                Text("No image")
                    .padding(.top, 20)
                
                Button("Make a photo") {
                    showingImagePicker = true
                }
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.blue, lineWidth: 1)
                )
                .padding(.top, 20)
            }
            
            if image != nil {
                Button("Reshoot") {
                    showingImagePicker = true
                }
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.blue, lineWidth: 1)
                )
                .padding()
                
                Button("Start verification process") {
                    isVerificationPresented = true
                }
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.blue, lineWidth: 1)
                )
                .padding(.top, 20)
                .fullScreenCover(isPresented: $isVerificationPresented) {
                    VerificationViewControllerRepresentable(showAlert: $showAlert, alertText: $alertText, actions: [.selfie], mode: mode, referencePhoto: image)
                        .ignoresSafeArea()
                }
                .alert("Result", isPresented: $showAlert) {
                    Button("OK", role: .cancel) { }
                } message: {
                    Text(alertText)
                }
            }
        }
        .sheet(isPresented: $showingImagePicker) {
            ImagePicker(selectedImage: $image)
        }
        .navigationTitle(title)
    }
}

#Preview {
    BioVerificationView(title: "Bio Verification", mode: .serverBased)
}
