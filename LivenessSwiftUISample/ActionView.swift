//
//  ActionView.swift
//  LivenessSwiftUISample
//
//  Created by Almir Akmalov on 04.04.2024.
//

import SwiftUI
import OZLivenessSDK

struct ActionView: View {
    @State private var currentAction: Action?
    @State private var showAlert = false
    @State private var alertText = ""
    
    var title: String
    var mode: AnalysisMode
    let actions = Action.allCases

    let bioVerification = "Bio Verification"
    
    var body: some View {
        List {
            ForEach(actions, id: \.self) { action in
                if action == .biometricCheck {
                    NavigationLink(destination: BioVerificationView(title: bioVerification, mode: mode)) {
                        Text(bioVerification)
                    }
                } else {
                    Button(action: {
                        currentAction = action
                    }) {
                        HStack {
                            Text(action.description)
                            Spacer()
                            Image(systemName: "chevron.right")
                                .foregroundColor(.gray)
                        }
                    }
                    .buttonStyle(PlainButtonStyle())
                }
            }
        }
        .navigationTitle(title)
        .fullScreenCover(item: $currentAction) { action in
            if action != .biometricCheck {
                let movement = action.toOZAction() ?? .smile
                VerificationViewControllerRepresentable(showAlert: $showAlert, alertText: $alertText, actions: [movement], mode: mode)
                    .ignoresSafeArea()
            }
        }
        .alert("Result", isPresented: $showAlert) {
            Button("OK", role: .cancel) { }
        } message: {
            Text(alertText)
        }
    }
}

#Preview {
    ActionView(title: "On server", mode: .serverBased)
}
