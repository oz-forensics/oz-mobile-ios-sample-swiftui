//
//  LivenessSwiftUISampleApp.swift
//  LivenessSwiftUISample
//
//  Created by Almir Akmalov on 04.04.2024.
//

import SwiftUI
import OZLivenessSDK

@main
struct LivenessSwiftUISampleApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        OZSDK(licenseSources: [.licenseFileName("forensics.license")]) { (result, error) in
            if let error = error {
                print(error)
                // license error
            }
        }
        OZSDK.journalObserver = { (value : String) -> () in
            //
        }
        OZSDK.localizationCode = .en
        OZSDK.attemptSettings = OZAttemptSettings(singleCount: 2,
                                                  commonCount: 3)
        
        OZSDK.customization.centerHintCustomization.verticalPosition = 10
        OZSDK.customization.centerHintCustomization.hideTextBackground = true
        
        let hintAnimationCustomization: HintAnimationCustomization = HintAnimationCustomization(
            hideAnimation: true,
            animationIconSize: 0,
            hintGradientColor: .clear
        )
        
        let faceFrameCustomization: FaceFrameCustomization = FaceFrameCustomization(
            strokeWidth: 4,
            strokeFaceAlignedColor: UIColor.systemGreen,
            strokeFaceNotAlignedColor: UIColor.systemRed,
            geometryType: .oval,
            strokePadding: 0
        )
        
        let backgroundCustomization = BackgroundCustomization(
            backgroundColor: UIColor.darkGray.withAlphaComponent(0.85)
        )
        
        OZSDK.customization.faceFrameCustomization = faceFrameCustomization
        OZSDK.customization.hintAnimationCustomization = hintAnimationCustomization
        OZSDK.customization.backgroundCustomization = backgroundCustomization
        
        return true
    }
}
