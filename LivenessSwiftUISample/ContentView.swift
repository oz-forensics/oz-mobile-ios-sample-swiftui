//
//  ContentView.swift
//  LivenessSwiftUISample
//
//  Created by Almir Akmalov on 04.04.2024.
//

import SwiftUI
import OZLivenessSDK

struct ContentView: View {
    @State private var isNavigationAllowed = false
    let onDevice = "On device"
    let hybrid = "hybrid"
    let onServer = "On server"
    
    var body: some View {
        if isNavigationAllowed {
            NavigationView {
                List {
                    NavigationLink(destination: ActionView(title: onDevice, mode: .onDevice)) {
                        Text(onDevice)
                    }
                    NavigationLink(destination: ActionView(title: hybrid, mode: .hybrid)) {
                        Text(hybrid)
                    }
                    NavigationLink(destination: ActionView(title: onServer, mode: .serverBased)) {
                        Text(onServer)
                    }
                }
                .navigationTitle("Mode")
            }
        } else {
                Text("Loading...")
                .onAppear {
                    login { (success) in
                        if success {
                            self.isNavigationAllowed = true
                        } else {
                            print("error")
                        }
                    }
                }
        }
    }
    
    func login(completion: @escaping (_ success: Bool) -> Void) {
        if OZSDK.isLoggedIn() {
            completion(true)
            return
        }
        let host = host
        let login = yourlogin
        let password = yourpassword
        OZSDK.setApiConnection(.fromCredentials(host: host, login: login, password: password)) { (token, error) in
            completion(token?.isEmpty == false)
        }
    }
}
#Preview {
    ContentView()
}
